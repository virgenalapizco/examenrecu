/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecu;


/**
 *
 * @author virge
 */
    public class VehiculoDeCarga extends Vehiculo {
    // Atributo específico de VehículoDeCarga
    private int toneladasCarga;

    // Constructor
    public VehiculoDeCarga(String serie, int tipoMotor, String marca, String modelo, int toneladasCarga) {
        super(serie, tipoMotor, marca, modelo); // Llama al constructor de la clase base Vehiculo
        this.toneladasCarga = toneladasCarga;
    }

    // Método getter para toneladasCarga
    public int getToneladasCarga() {
        return toneladasCarga;
    }

    // Método setter para toneladasCarga
    public void setToneladasCarga(int toneladasCarga) {
        this.toneladasCarga = toneladasCarga;
    }
}

