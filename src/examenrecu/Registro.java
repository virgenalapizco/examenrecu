/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecu;

/**
 *
 * @author virge
 */
import java.util.Date;

public class Registro {
    // Atributos
    private int numeroServicio;
    private Date fechaServicio;
    private int tipoServicio;
    private Vehiculo vehiculo;
    private String descripcionAutomovil;
    private double costoBaseMantenimiento;

    // Constructor
    public Registro(int numeroServicio, Date fechaServicio, int tipoServicio, Vehiculo vehiculo, String descripcionAutomovil, double costoBaseMantenimiento) {
        this.numeroServicio = numeroServicio;
        this.fechaServicio = fechaServicio;
        this.tipoServicio = tipoServicio;
        this.vehiculo = vehiculo;
        this.descripcionAutomovil = descripcionAutomovil;
        this.costoBaseMantenimiento = costoBaseMantenimiento;
    }

    // Getters y Setters
    public int getNumeroServicio() {
        return numeroServicio;
    }

    public void setNumeroServicio(int numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    public Date getFechaServicio() {
        return fechaServicio;
    }

    public void setFechaServicio(Date fechaServicio) {
        this.fechaServicio = fechaServicio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public double getCostoBaseMantenimiento() {
        return costoBaseMantenimiento;
    }

    public void setCostoBaseMantenimiento(double costoBaseMantenimiento) {
        this.costoBaseMantenimiento = costoBaseMantenimiento;
    }

    // Método para calcular el costo del servicio
    public double calcularCostoServicio() {
        // Aquí podrías implementar la lógica para calcular el costo total del servicio
        // Por ejemplo, podrías sumar el costo base más otros costos adicionales según el tipo de servicio, etc.
        return costoBaseMantenimiento;
    }
}
