/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecu;

/**
 *
 * @author virge
 */

    public class Vehiculo {
    // Atributos protegidos
    protected String serie;
    protected int tipoMotor;
    protected String marca;
    protected String modelo;

    // Constructor
    public Vehiculo(String serie, int tipoMotor, String marca, String modelo) {
        this.serie = serie;
        this.tipoMotor = tipoMotor;
        this.marca = marca;
        this.modelo = modelo;
    }

    // Métodos get y set para cada atributo
    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}


